package org.imie.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLite {
	
	private Connection connection;
	
	/** 
	 * Constructor
	 *  
	 * @throws ClassNotFoundException
	 */
	public SQLite() throws ClassNotFoundException {
		
		//load the sqlite-JDBC driver using the current class loader
	    Class.forName("org.sqlite.JDBC");
		
        try {        	
            // create a database connection
            this.connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
            
             Statement statement = connection.createStatement();
             statement.setQueryTimeout(30);  // set timeout to 30 sec.

             statement.executeUpdate("CREATE TABLE IF NOT EXISTS owner (id INTEGER(5) PRIMARY KEY, first_name VARCHAR(30), last_name VARCHAR(30));");
             statement.executeUpdate("CREATE TABLE IF NOT EXISTS account (id INTEGER(5) PRIMARY KEY, owner_id INTEGER(5), balance FLOAT);");
             
        }
        catch(SQLException e) {
             // if the error message is "out of memory", 
             // it probably means no database file is found
             System.err.println(e.getMessage());
        } 			
	}	
	
	/**
	 * Get connection
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {
		return this.connection;
	}
}