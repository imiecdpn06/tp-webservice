package org.imie.persistence;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {

	public abstract List<T> findAll() throws SQLException;
	
	public abstract T find(Integer id) throws SQLException;
	
	public abstract void insert(T entity) throws SQLException;
	
	public abstract void update(T entity) throws SQLException;
}