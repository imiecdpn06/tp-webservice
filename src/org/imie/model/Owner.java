package org.imie.model;

public class Owner {
	
	private Integer id;
	
	private String firstName;
	
	private String lastName;
	
	/**
	 * Constructor
	 * 
	 * @param Integer id
	 * @param String firstName
	 * @param String lastName
	 */
	public Owner(Integer id, String firstName, String lastName) {
		this.id        = id;
		this.firstName = firstName;
		this.lastName  = lastName;		
	}

	/**
	 * Get id
	 * 
	 * @return Integer
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set id
	 * 
	 * @param id
	 * @return void
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Get first name
	 * 
	 * @return String
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Set first name
	 * 
	 * @param String firstName
	 * @return void
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Get last name
	 * 
	 * @return String
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Set last name
	 * 
	 * @param String lastName
	 * @return void
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
