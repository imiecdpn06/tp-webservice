package org.imie.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.imie.persistence.DAO;

public class BankAccountDAO implements DAO<BankAccount> {

	private Connection connection;
	
	public BankAccountDAO(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	public List<BankAccount> findAll() throws SQLException {
		
		String sql = "SELECT * FROM account;";
		Statement statement = this.connection.createStatement();
		ResultSet results = statement.executeQuery(sql);        
        
        List<BankAccount> DTOList = new ArrayList<BankAccount>();               
        while(results.next()) {
            BankAccount newDTO = new BankAccount(
                results.getInt("id"),
                results.getInt("owner_id"),
                results.getFloat("balance")
            );
            DTOList.add(newDTO);
        }
        
        return DTOList;
	}

	@Override
	public BankAccount find(Integer id) throws SQLException {
		
		String sql = "SELECT * FROM account WHERE id = ?;";
		PreparedStatement statement = this.connection.prepareStatement(sql);
		statement.setInt(1, id);
		ResultSet results = statement.executeQuery();        
                              
        if(results.next()) {
            return new BankAccount(
                results.getInt("id"),
                results.getInt("owner_id"),
                results.getFloat("balance")
            );            
        }
        
        return null;
	}

	@Override
	public void insert(BankAccount bankAccount) throws SQLException {
		
		String sql = "INSERT INTO account(id, owner_id, balance) VALUES (?, ?, ?);";
		
		PreparedStatement statement = this.connection.prepareStatement(sql);
		statement.setInt(1, bankAccount.getId());
		statement.setInt(2, bankAccount.getOwnerId());
		statement.setFloat(3, bankAccount.getBalance());
		statement.executeUpdate();			
	}

	@Override
	public void update(BankAccount bankAccount) throws SQLException {
		
		String sql = "UPDATE account SET owner_id = ?, balance = ? WHERE id = ?;";
		
		PreparedStatement statement = this.connection.prepareStatement(sql);		
		statement.setInt(1, bankAccount.getOwnerId());
		statement.setFloat(2, bankAccount.getBalance());
		statement.setInt(3, bankAccount.getId());
		statement.executeUpdate();			
	}
}
