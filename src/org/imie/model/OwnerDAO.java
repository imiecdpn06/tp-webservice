package org.imie.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.imie.persistence.DAO;

public class OwnerDAO implements DAO<Owner> {

	private Connection connection;
	
	public OwnerDAO(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	public List<Owner> findAll() throws SQLException {

		String sql = "SELECT * FROM owner;";
		Statement statement = this.connection.createStatement();
		ResultSet results = statement.executeQuery(sql);   
        
        List<Owner> DTOList = new ArrayList<Owner>();               
        while(results.next()) {
            Owner newDTO = new Owner(
                results.getInt("id"),
                results.getString("first_name"),
                results.getString("last_name")
            );
            DTOList.add(newDTO);
        }
        
        return DTOList;		
	}

	@Override
	public Owner find(Integer id) throws SQLException {

		String sql = "SELECT * FROM owner WHERE id = ?;";
		PreparedStatement statement = this.connection.prepareStatement(sql);
		statement.setInt(1, id);
		ResultSet results = statement.executeQuery();        
        
        if(results.next()){
            return new Owner(
            	results.getInt("id"),
                results.getString("first_name"),
                results.getString("last_name")            		
            );
        }		
        
        return null;
	}

	@Override
	public void insert(Owner owner) throws SQLException {

		String sql = "INSERT INTO owner(id, first_name, last_name) VALUES (?, ?, ?);";
		
		PreparedStatement statement = this.connection.prepareStatement(sql);
		statement.setInt(1, owner.getId());
		statement.setString(2, owner.getFirstName());
		statement.setString(3, owner.getLastName());
		statement.executeUpdate();					
	}

	@Override
	public void update(Owner owner) throws SQLException {

		String sql = "UPDATE owner SET first_name = ?, last_name = ? WHERE id = ?;";
		
		PreparedStatement statement = this.connection.prepareStatement(sql);		
		statement.setString(1, owner.getFirstName());
		statement.setString(2, owner.getLastName());
		statement.setInt(3, owner.getId());
		statement.executeUpdate();			
		
	}
}
