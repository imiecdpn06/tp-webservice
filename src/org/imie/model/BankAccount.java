package org.imie.model;

public class BankAccount {
	
	private Integer id;
	
	private Integer ownerId;
	
	private Float balance;
	
	/**
	 * Constructor
	 * 
	 * @param Integer id
	 * @param Integer ownerId
	 * @param Integer balance
	 */
	public BankAccount(Integer id, Integer ownerId, Float balance) {
		this.id      = id;
		this.ownerId = ownerId;
		this.balance = balance;
	}

	/**
	 * Get id
	 * 
	 * @return Integer
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Set id
	 * 
	 * @param Integer id
	 * @return void
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Get owner id
	 * 
	 * @return Integer
	 */
	public Integer getOwnerId() {
		return this.ownerId;
	}

	/**
	 * Set owner id
	 * 
	 * @param Integer ownerId
	 * @return void
	 */
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Get balance
	 * 
	 * @return Float
	 */
	public Float getBalance() {
		return this.balance;
	}

	/**
	 * Set balance
	 *  
	 * @param Float balance
	 * @return void
	 */
	public void setBalance(Float balance) {
		this.balance = balance;
	}
	
	/**
	 * Transfer
	 * 
	 * @param Float amount
	 * @return void
	 */
	public void transfer(Float amount) {
		this.balance += amount;
	}
	
	/**
	 * Withdrawal
	 * 
	 * @param Float amount
	 * @return void
	 */
	public void withdrawal(Float amount) {
		this.balance -= amount; 
	}
}
