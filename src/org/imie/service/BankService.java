package org.imie.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.imie.model.BankAccount;
import org.imie.model.BankAccountDAO;
import org.imie.model.Owner;
import org.imie.model.OwnerDAO;
import org.imie.persistence.SQLite;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService()
public class BankService {

	private List<Owner> owners;	
	
	private Connection connection;
		
	private OwnerDAO ownerDao;
	
	private BankAccountDAO bankAccountDao;
	
	/**
	 * Constructor
	 * @throws ClassNotFoundException 
	 */
	public BankService() throws ClassNotFoundException {					
		this.connection     = new SQLite().getConnection();		
		this.bankAccountDao = new BankAccountDAO(this.connection);
		this.ownerDao       = new OwnerDAO(this.connection);
	}
	
	/**
	 * Create owner
	 * 
	 * @param Integer id
	 * @param String firstName
	 * @param String lastName
	 * @return Integer
	 * @throws SQLException 
	 */
	@WebMethod()
	public Integer createOwner(Integer id, String firstName, String lastName) throws SQLException {
		
		Owner owner = new Owner(id, firstName, lastName);
		this.ownerDao.insert(new Owner(id, firstName, lastName));								
		
//		return owner.getId();
		return this.ownerDao.find(id).getId();
	}
	
	/**
	 * Create bank account
	 * 
	 * @param Integer id
	 * @param Integer ownerId
	 * @param Float balance
	 * @return Integer
	 * @throws SQLException 
	 */
	@WebMethod()
	public Integer createBankAccount(Integer id, Integer ownerId, Float balance) throws SQLException {					
		
		if(this.ownerDao.find(ownerId) != null) {
			BankAccount bankAccount = new BankAccount(id, ownerId, balance);
			this.bankAccountDao.insert(bankAccount);
			
			return bankAccount.getId();
		}
			
		return null;	
	}	
	
	/**
	 * Get account balance
	 * 
	 * @param Integer accountId
	 * @return Float
	 * @throws SQLException 
	 */
	@WebMethod()
	public Float getAccountBalance(Integer accountId) throws SQLException {
				
		if(this.bankAccountDao.find(accountId) != null) {
			return this.bankAccountDao.find(accountId).getBalance();
		}
		
		return null;
	}
	
	/**
	 * Operation on account
	 * 
	 * @param String direction
	 * @param Float amount
	 * @param Integer accountId
	 * @return Float
	 * @throws SQLException 
	 */
	@WebMethod()
	public Float operationOnAccount(String direction, Float amount, Integer accountId) throws SQLException {
		
		if(this.bankAccountDao.find(accountId) != null) {
		
			BankAccount bankAccount = this.bankAccountDao.find(accountId);
			
			if(direction.equals("C")) {
				bankAccount.transfer(amount);
				this.bankAccountDao.update(bankAccount);
			}
			else if(direction.equals("D")) {
				bankAccount.withdrawal(amount);
				this.bankAccountDao.update(bankAccount);
			}	
			
			return bankAccount.getBalance();
		}
		
		return null;
	}
}
