package org.imie.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.imie.model.BankAccount;
import org.imie.model.BankAccountDAO;
import org.imie.model.Owner;
import org.imie.model.OwnerDAO;
import org.imie.persistence.SQLite;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection conn = new SQLite().getConnection();
		OwnerDAO odao = new OwnerDAO(conn);
		BankAccountDAO bdao = new BankAccountDAO(conn);
		
		BankService bs = new BankService();
		
		bs.createOwner(1, "Michael", "Jackson");		
		Owner owner = odao.find(1);		
		System.out.println(owner.getFirstName());
		
//		bs.createBankAccount(401, 1, 1000.0f);
//		BankAccount ba = bdao.find(401);
//		System.out.println(ba.getBalance());
/*		
		Float balance = bs.getAccountBalance(401);
		System.out.println(balance);
		
		Float transfer = bs.operationOnAccount('C', 50.0f, 401);		
		System.out.println(transfer);
		
		Float withdrawal = bs.operationOnAccount('D', 100.0f, 401);
		System.out.println(withdrawal);
*/		
		System.out.println("done");
	}
}